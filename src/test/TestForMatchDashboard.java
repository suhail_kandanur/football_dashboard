package test;

import static org.junit.Assert.*;

import org.junit.Test;

import football.MatchDashboard;
import football.status.StatusFlag;

/**
 * <code>TestForMatchDashboard</code> class covers the test cases for
 * <code>MatchDashboard</code> class. Some of these test cases overlap with
 * <TestForMatch> class but the input is provided in a string format
 * 
 * @author suhail
 *
 */
public class TestForMatchDashboard {

	/**
	 * Test that leads to success, this is a straight forward case
	 */
	@Test
	public void testForSuccess() {
		MatchDashboard _dashboard = new MatchDashboard();
		String input = "Start: England vs. Germany";
		int statusCode = _dashboard.dispatch(input);
		assert (StatusFlag.isFlagSet(statusCode, StatusFlag.SC_OK));
		input = "print";
		statusCode = _dashboard.dispatch(input);
		assert (StatusFlag.isFlagSet(statusCode, StatusFlag.SC_OK));
	}

	/**
	 * Test to evaluate if the code failes starting a match which is already in progress
	 */
	@Test
	public void testForDoubleStart() {
		MatchDashboard _dashboard = new MatchDashboard();
		String input = "Start: England vs. 'West Germany'";
		int statusCode = _dashboard.dispatch(input);
		assert (StatusFlag.isFlagSet(statusCode, StatusFlag.SC_OK));

		// dispatch the same command to start an existing match in progress
		// this is a failure scenario
		statusCode = _dashboard.dispatch(input);
		assert (StatusFlag
				.isFlagSet(statusCode, StatusFlag.SC_GAME_IN_PROGRESS));
		assert (StatusFlag.isFlagSet(statusCode, StatusFlag.SC_FAIL));
	}

	/**
	 * This will test the number of goals made by each team
	 */
	@Test
	public void testGoals() {
		MatchDashboard _dashboard = new MatchDashboard();
		String input = "Start: England vs. 'West Germany'";
		int statusCode = _dashboard.dispatch(input);
		//the start operation should succeed, and the match should be in progress  
		assert(StatusFlag.isFlagSet(statusCode, StatusFlag.SC_OK));
		assert(StatusFlag.isFlagSet(statusCode, StatusFlag.SC_GAME_IN_PROGRESS));
		String[] goals  = {"1 England John", "13 England Scott", "55 Germany Fred", "19 Germany Tom", "44 Germany Fred"};
		for (String s : goals) {
			statusCode = _dashboard.dispatch(s);
			//operation to register a goal should succeed
			assert(StatusFlag.isFlagSet(statusCode, StatusFlag.SC_OK));
		}
		
		input = "End";
		statusCode = _dashboard.dispatch(input);
		assert(StatusFlag.isFlagSet(statusCode, StatusFlag.SC_OK));
		input = "13 Germany Tom";
		statusCode = _dashboard.dispatch(input);
		assert(StatusFlag.isFlagSet(statusCode, StatusFlag.SC_MATCH_ENDED));
		assert(StatusFlag.isFlagSet(statusCode, StatusFlag.SC_FAIL));

	}

}
