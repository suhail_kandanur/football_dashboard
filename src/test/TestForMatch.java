package test;

import static org.junit.Assert.*;

import org.junit.Test;

import football.valueobjects.Match;

/**
 * <code>TestForMatch</code> class covers the test cases for <code>Match</code>
 * object
 * 
 * @author suhail
 *
 */
public class TestForMatch {

	/**
	 * Test the initialization of <code>Match</code> object.
	 * 
	 */
	@Test
	public void testInitialization() {
		Match _match = new Match();
		_match.initialize("England", "Germany");
		assertEquals("England", _match.getHomeTeam());
		assertEquals("Germany", _match.getAwayTeam());
		assert(!_match.isMatchInProgress());
		_match.startMatch();
		assert(_match.isMatchInProgress());
	}

	/**
	 * Test the scenarios during the match in progress
	 */
	@Test
	public void testNoOfGoals() {
		Match _match = new Match();
		_match.initialize("England", "Germany");
		assertEquals("England", _match.getHomeTeam());
		assertEquals("Germany", _match.getAwayTeam());

		assert (!_match.isMatchInProgress());
		_match.startMatch();
		assert (_match.isMatchInProgress());
		_match.registerGoal(1, "England", "John");
		_match.registerGoal(13, "England", "Scott");
		_match.registerGoal(55, "Germany", "Fred");
		_match.registerGoal(19, "Germany", "Tom");
		_match.registerGoal(44, "Germany", "Fred");
		System.out.println(_match.goalsForTeam("England"));
		assertEquals(2, _match.goalsForTeam("England"));
		assertEquals(3, _match.goalsForTeam("Germany"));
	}

	/**
	 * Test the cases before and after the match ends
	 */
	@Test
	public void testEnd() {
		Match _match = new Match();
		_match.initialize("England", "Germany");
		assertEquals("England", _match.getHomeTeam());
		assertEquals("Germany", _match.getAwayTeam());

		assert (!_match.isMatchInProgress());
		_match.startMatch();
		assert (_match.isMatchInProgress());

		assert (!_match.isMatchEnded());
		_match.endMatch();
		assert (_match.isMatchEnded());

		// cannot register the goal after the match has ended
		boolean status = _match.registerGoal(44, "Germany", "Fred");
		assert (!status);

	}

}
