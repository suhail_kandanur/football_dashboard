package test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Football Dashboard Test Suite
 * 
 * @author suhail
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ TestForMatch.class, TestForMatchDashboard.class,
		TestForGoal.class })
public class AllTests {

}
