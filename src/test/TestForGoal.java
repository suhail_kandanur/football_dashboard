package test;

import static org.junit.Assert.*;

import org.junit.Test;

import football.valueobjects.Goal;

/**
 * <code>TestForGoal</code> class covers the test cases for <code>Goal</code>
 * class.
 * 
 * @author suhail
 *
 */
public class TestForGoal {

	@Test
	public void testAccessMethods() {
		Goal g = new Goal(10, "John");
		assertEquals(g.getMinute(), 10);
		assertEquals(g.getPlayer(), "John");
		assertEquals(g.toString(), "John 10' ");
	}

}
