package football.status;

/**
 * <code>StatusFlag</code> class encapsulates the various states of the match
 * and the outcome of operations based on input commands. These states are
 * useful for consuming code to determine the exact state of the match.
 * 
 * <p>
 * The codes are chosen in a way to capture multiple states in to a statusCode.
 * Bitwise ORing multiple status codes will capture the codes into a single code
 * and can be cross checked when needed using isFlagSet method
 * 
 * @author suhail
 *
 */
public class StatusFlag {
	public static int SC_OK = 0x00000001;
	public static int SC_NO_GAME_IN_PROGRESS = 0x00000010;
	public static int SC_GAME_IN_PROGRESS = 0x00000100;
	public static int SC_INPUT_ERR = 0x00001000;
	public static int SC_FAIL = 0x10000000;
	public static int SC_MATCH_ENDED = 0x01000000;
	public static int SC_UNKNOWN = 0x00100000;

	/**
	 * Method to check if a particular status code has been captured in the status.
	 * For eg, if the operation fails with 'Game in progress'
	 * the status code will be set as SC_FAIL | SC_GAME_IN_PROGRESS, and 
	 * isFlagSet(status, SC_GAME_IN_PROGRESS) and isFlagSet(status, SC_FAIL) will both return true.
	 * 
	 * @param status
	 * @param flag
	 * @return
	 */
	public static boolean isFlagSet(int status, int flag) {
		return (status & flag) == flag;
	}

	public static int setFlag(int status, int flag) {
		return (status | flag);
	}

}
