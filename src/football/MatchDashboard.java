package football;

import java.util.regex.Pattern;

import football.commands.CmdFactory;
import football.commands.IMatchCommand;
import football.status.StatusFlag;
import football.valueobjects.Match;

/**
 * <code>MatchDashboard</code> class encapsulates the main application logic to
 * handle supplied commands and change the state of <code>Match</code> object
 * 
 * @author suhail
 *
 */
public class MatchDashboard {
	private Match _match;

	/**
	 * Default constructor for this class, which create an instance of
	 * <code>Match</code> class however, the <code>Match</code> object is not
	 * fully initialized at this point of time.
	 */
	public MatchDashboard() {
		_match = new Match();
	}

	/**
	 * Handles the input supplied by mapping the input to a object of type
	 * <code>IMatchCommand</code>. <code>CmdFactory</code> is a factory that
	 * creates specialized commands that will perform operations on
	 * <code>Match</code> object
	 * 
	 * @param input
	 * @return
	 */
	public int dispatch(String input) {
		int statusCode = StatusFlag.SC_UNKNOWN; //status is unknown at this point of time
		// this uses the Factory pattern to instantiate command objects
		// and Command pattern to dispatch commands based on the input.
		IMatchCommand cmd = CmdFactory.getCommand(input);
		if (cmd != null) {
			statusCode = cmd.execute(_match);
			printStatus(statusCode);
		} else {
			// the supplied command is not a valid one and cannot be handled
			// print out the error message depending on the state of the match
			if (_match != null && _match.isMatchInProgress()) {
				System.err
						.println("input error - please type 'print' for game details");
				//input error and 'Game in progress' captured as status code
				statusCode = StatusFlag.SC_INPUT_ERR | StatusFlag.SC_GAME_IN_PROGRESS;
			} else {
				System.err
						.println("input error - please start a game through typing 'Start: '<Name of Home Team>' vs. '<Name of Away Team>'");
				//input error and 'Game Not in progress" captured
				statusCode = StatusFlag.SC_INPUT_ERR | StatusFlag.SC_NO_GAME_IN_PROGRESS;
			}
			
		}
		return statusCode;
	}

	/**
	 * Method to print out status messages based on the outcome of the command
	 * executions
	 * 
	 * @param statusCode
	 */
	private void printStatus(int statusCode) {
		//if the game is not in progress
		if (StatusFlag.isFlagSet(statusCode, StatusFlag.SC_NO_GAME_IN_PROGRESS)) {
			System.err.println("No game currently in progress");
		}
		
		// the game is in progress but the command cannot be handled because 
		if (StatusFlag.isFlagSet(statusCode, StatusFlag.SC_FAIL)
				&& StatusFlag.isFlagSet(statusCode,
						StatusFlag.SC_GAME_IN_PROGRESS))
			System.err.println("A game is already in progress");

	}

}
