package football.valueobjects;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * <code>Match</code> class is little more than a POJO that provides methods to
 * change the state of a match. If initializes, starts, registers goals &
 * terminates the match.
 * 
 * 
 * @author suhail
 *
 */
public class Match {

	private String _teamHome;
	private String _teamAway;
	private Map<String, List<Goal>> _goals;
	private boolean _matchInProgress;
	private boolean _matchEnded;

	public Match() {

	}

	/**
	 * Initializes the match with Home and Away team names, it also creates data
	 * structure to maintain the state of the match.
	 * 
	 * @param teamA_
	 * @param teamB_
	 */
	public void initialize(String teamA_, String teamB_) {
		this._teamHome = teamA_;
		this._teamAway = teamB_;
		this._matchInProgress = false;
		this._matchEnded = false;
		this._goals = new HashMap<String, List<Goal>>();
	}

	public String getHomeTeam() {
		return this._teamHome;
	}

	public String getAwayTeam() {
		return this._teamAway;
	}

	/**
	 * Starts the match
	 */
	public void startMatch() {
		this._matchInProgress = true;
	}

	/**
	 * End the match
	 */
	public void endMatch() {
		this._matchInProgress = false;
		this._matchEnded = true;
	}

	public boolean isMatchInProgress() {
		return this._matchInProgress;
	}

	public boolean isMatchEnded() {
		return this._matchEnded;
	}

	/**
	 * To register the goals for a team & player. If the team name does not
	 * match with the initialized team names, it will return false which implies
	 * a failure to register a goal
	 * 
	 * @param min
	 * @param team
	 * @param player
	 * @return
	 */
	public boolean registerGoal(int min, String team, String player) {
		// goal cannot be registered,
		// unless the match is in progress, and
		// the team specified matches either Home or Away team
		if (!_matchInProgress)
			return false;
		if (team == null
				|| "".equals(team)
				|| (!team.equalsIgnoreCase(_teamHome) && !team
						.equalsIgnoreCase(_teamAway)))
			return false;

		// initialize the goal list if its not already initialized
		// we're doing delayed instantiation of the ArrayList as there might be
		// a possibility of any team
		// scoring zero goals
		List<Goal> goalList = _goals.get(team);
		if (goalList == null) {
			goalList = new ArrayList<Goal>();
			this._goals.put(team, goalList);
		}

		goalList.add(new Goal(min, player));
		return true;
	}

	/**
	 * This is a little complex method to print stats of a match. It prints the
	 * goals in time order even if the goals are registered in random order. It
	 * uses lambdas & comparator to sort the goals in time order. JDK 1.8 is
	 * required
	 * 
	 * @return
	 */

	public String stats() {
		// we use a comparator to sort the goals as per the time order in which
		// they were scored
		Comparator<Goal> goalsByMinute = (g1, g2) -> Integer.compare(
				g1.getMinute(), g2.getMinute());
		StringBuilder stats = new StringBuilder(this._teamHome);
		List<Goal> teamAGoals = _goals.get(_teamHome);
		List<Goal> teamBGoals = _goals.get(_teamAway);
		if (teamAGoals != null && teamAGoals.size() > 0) {
			stats.append(" ");
			stats.append(teamAGoals.size());
			stats.append(" (");
			// sort the list based on time order, and
			// append to the string
			teamAGoals.stream().sorted(goalsByMinute)
					.forEach(g -> stats.append(g.toString()));
			stats.append(")");
		} else {
			stats.append(" 0 ");
		}
		stats.append(" vs. ");
		stats.append(this._teamAway);
		if (teamBGoals != null && teamBGoals.size() > 0) {
			stats.append(" ");
			stats.append(teamBGoals.size());
			stats.append(" (");
			// sort the list of goals for teamAway based on time order, and
			// append to the string
			teamBGoals.stream().sorted(goalsByMinute)
					.forEach(g -> stats.append(g.toString()));
			stats.append(")");
		} else {
			stats.append(" 0 ");
		}
		return stats.toString();
	}

	public int goalsForTeam(String team) {
		return (_goals.get(team) != null) ? _goals.get(team).size() : 0;
	}
}
