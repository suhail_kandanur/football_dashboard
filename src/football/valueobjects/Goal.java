package football.valueobjects;

/**
 * <code>Goal</code> is a POJO to capture information about a goal
 * 
 * @author suhail
 *
 */
public class Goal {
	private int _minute;
	private String _player;

	public Goal(int minute, String player) {
		this._minute = minute;
		this._player = player;
	}

	public int getMinute() {
		return _minute;
	}

	public String getPlayer() {
		return _player;
	}

	public String toString() {
		StringBuilder gStr = new StringBuilder(this._player);
		gStr.append(" ");
		gStr.append(this._minute);
		gStr.append("' ");
		return gStr.toString();
	}

}
