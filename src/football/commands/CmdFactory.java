package football.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * <code>CmdFactory</code> is a factory class that instantiates specialized
 * <code>IMatchCommand</code> objects based on the input provided
 * 
 * @author suhail
 *
 */
public class CmdFactory {

	/**
	 * Takes a command string, validates and instantiates object of type
	 * <code>IMatchCommand</code> If the input is invalid or cannot be
	 * understood, it returns null
	 * 
	 * @param cmd
	 * @return
	 */
	public static IMatchCommand getCommand(String cmd) {
		if (cmd == null || "".equals(cmd))
			return null;
		Scanner scanner = new Scanner(cmd).useDelimiter("\\s+");
		List<String> tokenList = new ArrayList<String>();
		while (scanner.hasNext()) {
			String token = scanner.next();
			StringBuilder tokenBuilder = new StringBuilder(token);
			// if double quotes are used to specify multiple values,
			// they have to be considered as single token
			if (token.indexOf("\"") == 0) {
				while ((token.indexOf("\"", 1) != token.length() - 1)
						&& scanner.hasNext()) {
					token = scanner.next();
					tokenBuilder.append(" ");
					tokenBuilder.append(token);
				}
				if (tokenBuilder.toString().indexOf("\"", 1) < 0) {
					return null;
				}
			} else
			// if single quotes are used to specify multiple values,
			// they have to be considered as single token
			if (token.indexOf("'") == 0) {
				while ((token.indexOf("'", 1) != token.length() - 1)
						&& scanner.hasNext()) {
					token = scanner.next();
					tokenBuilder.append(" ");
					tokenBuilder.append(token);
				}
				if (tokenBuilder.toString().indexOf("'", 1) < 0) {
					return null;
				}
			}
			String cleanToken = tokenBuilder.toString();
			//trim leading and trailing quotes
			cleanToken = cleanToken.replaceAll("^\"|\"$",  "");
			cleanToken = cleanToken.replaceAll("^'|'$",  "");
			tokenList.add(cleanToken);
			
		}

		String[] tokens = new String[tokenList.size()];
		tokens = tokenList.toArray(tokens);
		String op = tokens[0];
		if (op == null || "".equals(op))
			return null;

		// Start command should have the following syntax:
		// Start: England vs. Germany
		if ("Start:".equalsIgnoreCase(op) && tokens.length == 4) {
			return new StartCmd(tokens[1], tokens[3]);
		} else
		// Command to register a goal has the following syntax:
		// 11 England Peter (minute, team, player)
		if (Pattern.matches("\\d+", op) && tokens.length == 3) {
			int min;
			try {
				min = Integer.valueOf(op);
			} catch (NumberFormatException nfe) {
				return null;
			}
			return new GoalScoreCmd(min, tokens[1], tokens[2]);
		} else
		// End command
		if ("End".equalsIgnoreCase(op)) {
			return new EndCmd();
		} else
		// Print command
		if ("print".equalsIgnoreCase(op)) {
			return new PrintCmd();
		}
		return null;
	}

}
