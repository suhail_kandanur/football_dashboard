package football.commands;

import football.status.StatusFlag;
import football.valueobjects.Match;

/**
 * <code>GoaldScoreCmd</code> class registers a goal in a match if its in
 * progress. This is a specialization of <code>IMatchCommand</code>
 * 
 * @author suhail
 *
 */
public class GoalScoreCmd implements IMatchCommand {

	private String _team;
	private int _min;
	private String _player;

	public GoalScoreCmd(int min, String team, String player) {
		this._min = min;
		this._team = team;
		this._player = player;
	}

	@Override
	public int execute(Match m) {
		// a goal can only be scored when the match is in progress and has not ended
		if (m != null && m.registerGoal(this._min, this._team, this._player))
			return StatusFlag.setFlag(StatusFlag.SC_OK,
					StatusFlag.SC_GAME_IN_PROGRESS);
		return StatusFlag.SC_NO_GAME_IN_PROGRESS | StatusFlag.SC_FAIL;

	}
}
