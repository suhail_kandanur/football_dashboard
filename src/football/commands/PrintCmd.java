package football.commands;

import football.status.StatusFlag;
import football.valueobjects.Match;

/**
 * <code>PrintCmd</code> class handles the "print" command and display the match
 * stats.
 * 
 * @author suhail
 *
 */
public class PrintCmd implements IMatchCommand {

	@Override
	public int execute(Match m) {
		// stats can only be displayed if the match has started or has ended
		// with a previous state of progress
		if (m != null && (m.isMatchInProgress() || m.isMatchEnded())) {
			System.out.println(m.stats());
			return StatusFlag.setFlag(StatusFlag.SC_OK,
					StatusFlag.SC_GAME_IN_PROGRESS);
		} else {
			return StatusFlag.SC_NO_GAME_IN_PROGRESS | StatusFlag.SC_FAIL;
		}

	}

}
