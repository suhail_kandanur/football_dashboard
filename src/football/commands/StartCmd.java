package football.commands;

import football.status.StatusFlag;
import football.valueobjects.Match;

/**
 * <code>StartCmd</code> class starts the match.
 * @author suhail
 *
 */
public class StartCmd implements IMatchCommand {

	private String _teamA;
	private String _teamB;
	public StartCmd(String teamA, String teamB) {
		this._teamA = teamA;
		this._teamB = teamB;
	}
	@Override
	public int execute(Match m) {
		//a match can only be started if its already not started
		if(m == null || m.isMatchInProgress()) {
			return StatusFlag.SC_GAME_IN_PROGRESS | StatusFlag.SC_FAIL;
		}
		
		//initialize the teams and start the match
		m.initialize(this._teamA, this._teamB);
		m.startMatch();
		return StatusFlag.setFlag(StatusFlag.SC_OK, StatusFlag.SC_GAME_IN_PROGRESS);
		
	}

}
