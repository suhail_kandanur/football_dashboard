package football.commands;

import football.status.StatusFlag;
import football.valueobjects.Match;

/**
 * <code>EndCmd</code> class is a specialization of <code>IMatchCommand</code>
 * that handle "End" command to end a football match
 * 
 * @author suhail
 *
 */
public class EndCmd implements IMatchCommand {

	@Override
	public int execute(Match m) {
		//a match can only be ended, if its in progress
		if (m!= null && m.isMatchInProgress()) {
			m.endMatch();
			return StatusFlag.SC_OK | StatusFlag.SC_MATCH_ENDED;
		} else {
			return StatusFlag.SC_NO_GAME_IN_PROGRESS | StatusFlag.SC_FAIL;
		}

	}

}
