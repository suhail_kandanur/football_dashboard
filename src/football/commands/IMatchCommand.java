package football.commands;

import football.valueobjects.Match;

/**
 * <code>IMatchCommand</code> is an interface that will be implemented by
 * specialized commands to change the state of the match.
 * <code>CmdFactory</code> factory class instantiates object of this type
 * depending on the specified input
 * 
 * @author suhail
 *
 */
public interface IMatchCommand {
	public int execute(Match m);
}
