package football;

import java.io.Console;

/**
 * The <code>Main</code> class is the entry point for this application.
 * 
 * <p>
 * It reads the input as String from console, cleans up leading and trailing
 * whitespaces before forwarding it to the Football Match Dashboard which
 * ultimately processes the input.
 * 
 * @author suhail
 *
 */
public class Main {

	/**
	 * Application bootstrap
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Console cons = null;
		String input = null;
		MatchDashboard dashboard = new MatchDashboard();
		while (true) {
			if (cons == null)
				cons = System.console();
			if ((input = cons.readLine()) != null) {
				// cleanup input
				input = input.trim();

				// this is the only command which is handled at this point
				// this will allow us to exit the while loop graciously
				if ("quit".equalsIgnoreCase(input)) {
					break;
				}
				try {
					dashboard.dispatch(input);
				} catch (Exception ex) {
					// the football dashboard application doesn't throw any
					// checked exception
					// this is just a safety net to avoid exiting the
					// application in case of errors.
					System.err
							.println("Unexpected error, strace trace follows: ");
					ex.printStackTrace();
					continue;
				}
			} else {
				// this is a rare condition. but we'll handle it anyways.
				System.err.println("input is null.");
				continue;
			}
		}

	}

}
